export class Teacher {
    id: number;
    name: string;
    age: number;
    approved: boolean;
}