import { Teacher } from './teacher';

export class User {
    accessToken: string;
    expiresIn: number;
    id: number;
    name: string;
    username: string;
    type: string;
    email: string;
    password: string;
    teacher: Teacher;
}