import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../service/user.service';
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public user: User = new User();

  constructor(private router: Router,
              private userService: UserService) { }

  ngOnInit() {
  }

  public login(user: User): void {
    if (!user) return;

    this.userService.login(user).subscribe(
      data => {
        if (data.type === 'admin' && data.accessToken) {
          localStorage.setItem("access_token", data.accessToken);
          this.router.navigateByUrl("/teachers");
        } else {
          console.log('error!');
        }
      },
      (error: any) => console.log(error)
    );
  }
}
