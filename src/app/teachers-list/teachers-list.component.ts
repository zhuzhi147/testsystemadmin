import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../service/user.service';
import { Observable } from 'rxjs';
import { Teacher } from '../models/teacher';
import { Router } from "@angular/router";

@Component({
  selector: 'app-teachers-list',
  templateUrl: './teachers-list.component.html',
  styleUrls: ['./teachers-list.component.css']
})
export class TeachersListComponent implements OnInit {
  public teachers$: Observable<User[]>;

  constructor(private readonly userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.teachers$ = this.userService.getTeachers();
  }

  public updateTeacher(teacher: Teacher, status: boolean): void {
    teacher.approved = true;
    this.userService.updateTeacherStatus(teacher)
        .subscribe(() => this.teachers$ = this.userService.getTeachers());
  }

  logout(): void {
    this.userService.logout(this.router);
  }

}
