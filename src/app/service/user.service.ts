import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { Observable } from 'rxjs';
import { API_URL } from 'src/environments/environment';
import { Teacher } from '../models/teacher';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<User> {
    return this.http.post<User>(`${API_URL.AUTH}/login`, user)
  }

  public getTeachers(): Observable<User[]> {
    return this.http.get<User[]>(`${API_URL.USER}`)
  }

  public logout(router: Router): void {
    localStorage.removeItem('access_token');
    router.navigateByUrl('login');
  }

  public updateTeacherStatus(teacher: Teacher): Observable<Teacher> {
    return this.http.put<Teacher>(`${API_URL.TEACHER}`, teacher);
  }

}
